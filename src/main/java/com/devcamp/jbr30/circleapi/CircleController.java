package com.devcamp.jbr30.circleapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double  getCircleArea(){
        
        Circle circle = new Circle(4.2);
       
       
        System.out.println(circle);
        
        return circle.getArea();
        

    }
}
